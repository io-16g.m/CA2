#include <linux/init.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/moduleparam.h>
#include <linux/list.h>
#include <linux/ktime.h>
#include <linux/slab.h>


MODULE_AUTHOR("IO-16 Girlya Maksym");
MODULE_DESCRIPTION("Print \"Hello World\" determined by howmany parameter times. Show kernel time of initial print on rmmod");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_PARM_DESC(howmany, "An unsigned int to determine how many times 'Hello, world!' is printed");


struct my_list_entry {
	struct list_head list;
	ktime_t time;
};

static LIST_HEAD(my_list_head);
static unsigned int howmany = 1;
static unsigned int counter = 0;
module_param(howmany, uint, 0660);


static int __init hello_init(void)
{
	struct my_list_entry *new_entry;

	if (howmany > 10) {
	printk(KERN_ERR "Error: 'howmany' is greater than 10.\n");
	return -EINVAL;
	}

	if (howmany == 0 || (howmany >= 5 && howmany <= 10))
	printk(KERN_WARNING "Warning: 'howmany' is %u. It should be between 1 and 4.\n",
		howmany);
	
	counter = howmany;

	while (counter--) {
		new_entry = kmalloc(sizeof(*new_entry), GFP_KERNEL);
		if (!new_entry)
			return -ENOMEM;

		new_entry->time = ktime_get();
		list_add_tail(&new_entry->list, &my_list_head);
		printk(KERN_INFO "Hello, world!\n");
	}

	return 0;
}

static void __exit hello_exit(void)
{
	struct my_list_entry *current_entry, *next_entry;

	list_for_each_entry_safe(current_entry, next_entry,
				&my_list_head, list) {
		printk(KERN_INFO "Time: %lld ns\n",
			ktime_to_ns(current_entry->time));
		list_del(&current_entry->list);
		kfree(current_entry);
		}

	printk(KERN_ALERT "Goodbye, cruel world\n");
}

module_init(hello_init);
module_exit(hello_exit);
