#ifndef HELLO1_H
#define HELLO1_H

#include <linux/ktime.h>
#include <linux/list.h>

struct my_list_entry {
    struct list_head list;
    ktime_t time_before;
    ktime_t time_after;
};

void print_hello(unsigned int times);

#endif // HELLO1_H
