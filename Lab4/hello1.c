#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/printk.h>
#include "hello1.h"

MODULE_AUTHOR("IO-16 Girlya Maksym");
MODULE_DESCRIPTION("hello1 module part");
MODULE_LICENSE("Dual BSD/GPL");

static LIST_HEAD(my_list_head);

void print_hello(unsigned int times) {
    struct my_list_entry *entry;
    unsigned int i;

    for (i = 0; i < times; i++) {
        entry = kmalloc(sizeof(*entry), GFP_KERNEL);
        if (!entry) {
            pr_err("Memory allocation failed\n");
            return;
        }

        entry->time_before = ktime_get();
        pr_info("Hello, world!\n");
        entry->time_after = ktime_get();

        list_add_tail(&entry->list, &my_list_head);
    }
}

static int __init hello1_init(void) {
    pr_info("hello1 module loaded\n");
    return 0;
}

static void __exit hello1_exit(void) {
    struct my_list_entry *entry, *tmp;

    list_for_each_entry_safe(entry, tmp, &my_list_head, list) {
        pr_info("Time spent: %lld ns\n", ktime_to_ns(entry->time_after) - ktime_to_ns(entry->time_before));
        list_del(&entry->list);
        kfree(entry);
    }

    pr_info("hello1 module unloaded\n");
}

module_init(hello1_init);
module_exit(hello1_exit);

EXPORT_SYMBOL(print_hello);
