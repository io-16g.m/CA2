#include <linux/init.h>
#include <linux/module.h>
#include "hello1.h"

MODULE_AUTHOR("IO-16 Girlya Maksym");
MODULE_DESCRIPTION("hello2 module part");
MODULE_LICENSE("Dual BSD/GPL");

static unsigned int howmany = 1;
module_param(howmany, uint, 0660);
MODULE_PARM_DESC(howmany, "An unsigned int to determine how many times 'Hello, world!' is printed");

static int __init hello2_init(void) {
    pr_info("hello2 module loaded\n");

    if (howmany > 10) {
        pr_err("Error: 'howmany' is greater than 10.\n");
        return -EINVAL;
    }

    if (howmany == 0 || (howmany >= 5 && howmany <= 10)) {
        pr_warn("Warning: 'howmany' is %u. It should be between 1 and 4.\n", howmany);
    }

    print_hello(howmany);
    return 0;
}

static void __exit hello2_exit(void) {
    pr_info("hello2 module unloaded\n");
}

module_init(hello2_init);
module_exit(hello2_exit);
